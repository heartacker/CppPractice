template <class T>
class CARRAY_2DIM
{
public:
    T **pt;
    int x;
    int y;

    CARRAY_2DIM(int m, int n)
    {
        x = m;
        y = n;
        {
            pt = new T *[m];
            for (int i = 0; i < m; i++)
            {
                pt[i] = new T[n];
            }
        }
    }
    ~CARRAY_2DIM()
    {
        for (int i = 0; i < x; i++)
        {
            delete[] pt[i];
        }
        delete[] pt;
        pt = NULL;
    }
    operator T **()
    {
        return pt;
    }
    T *operator[](int a)
    {
        return pt[a];
    }
};