#include "stdlib.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"

#include "tmu.h"

#define SiteCnt 4
uint32_t g_readValue[SiteCnt] = {0};

void ReadPattern(uint32_t addr)
{
    for (uint32_t i = 0; i < SiteCnt; i++)
    {
        g_readValue[i] = addr;
    }
}

void ReadMem(uint32_t addr, uint32_t len, uint32_t **data1)
{
    printf("%p\n", data1);
    for (int i = 0; i < len; i++)
    {
        ReadPattern(addr + i * 4);
        for (uint32_t SOCKET = 0; SOCKET < SiteCnt; SOCKET++)
        {

#if 0
            // data1[i][SOCKET] = g_readValue[SOCKET];
            ((uint32_t *)data1)[(len * SOCKET) + i] = g_readValue[SOCKET];
#else
            // data1[SOCKET][i] = g_readValue[SOCKET];
            ((uint32_t *)data1)[(SiteCnt * i) + SOCKET] = g_readValue[SOCKET];
#endif // FALSE
        }
    }
}
void ReadMem(uint32_t addr, uint32_t len, CARRAY_2DIM<uint32_t> data1)
{
    printf("%p\n", data1);
    for (int i = 0; i < len; i++)
    {
        ReadPattern(addr + i * 4);
        for (uint32_t SOCKET = 0; SOCKET < SiteCnt; SOCKET++)
        {
            data1[SOCKET][i] = g_readValue[SOCKET];
        }
    }
}

void JDUGE_MS(uint32_t *data, uint32_t low, uint32_t high, const char *title)
{
    for (size_t SOCKET = 0; SOCKET < SiteCnt; SOCKET++)
    {
        if ((data[SOCKET] < low) || (data[SOCKET] > high))
        {
            printf("Fail ");
        }
        else
        {
            printf("pass ");
        }
        printf("%s[%d] = 0x%08X\n", title, SOCKET, data[SOCKET]);
    }
}

int main(int argc, char const *argv[])
{
// 需要读写 3个寄存器
#define CNT 3
    uint32_t data[CNT][SiteCnt];
    memset(data, 0, sizeof(data));

    printf("hello world\r\n");
    printf("%p\n", data);
    ReadMem(0x40000000, CNT, (uint32_t **)data);
    printf("hello world2\r\n");

    for (size_t i = 0; i < CNT; i++)
    {
        JDUGE_MS(data[i], 0, 50, "READMEN");
    }

#if 1
    printf("hello world3\r\n");
    CARRAY_2DIM<uint32_t> data1(SiteCnt, CNT);
    ReadMem(0x40000000, CNT, data1);
    printf("hello world4\r\n");

    for (size_t i = 0; i < CNT; i++)
    {
        // 这里要把3个寄存器一次打印出了，每次打印 同一个地址的不同site。但是下面是错误的。
        //! data1[1] 获取的是一个site的所有值。而不是一个地址的多个site
        JDUGE_MS(data1[i], 0, 0x40000000 + i * 4, "READMEN");
    }
#endif // 0
#if 0
    printf("hello world5\r\n");
    CARRAY_2DIM<uint32_t> data2(SiteCnt, CNT);
    ReadMem(0x40000000, CNT, (uint32_t **)(**data2));
    printf("hello world6\r\n");

    for (size_t i = 0; i < CNT; i++)
    {
        // 这里要把3个寄存器一次打印出了，每次打印 同一个地址的不同site。但是下面是错误的。
        //! data2[1] 获取的是一个site的所有值。而不是一个地址的多个site
        JDUGE_MS(data2[i], 0, 0x40000000 + i * 4, "READMEN");
    }
#endif // 0

    return 0;
}

#if FALSE

char HEX_buf[16][5] = {"0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111"};

int main()
{
    int L1;
    printf("输入数字:");
    scanf("%d", &L1);
    char str1[6] = "123456";
    printf(str1);
    char str2[256] = "1";
    char str3[256];
    char str4[256];
    itoa(L1, str1, 2);
    itoa(L1, str2, 8);
    itoa(L1, str3, 16);
    strcpy(str4, HEX_buf[L1]);
    printf("2进制:%s\t8进制:%s\t16进制:%s\t", str1, str2, str3);
    printf("2进制:%s\n", str4);
    printf("二进制 长度%d\n", sizeof(str1));

    char _4bits[5];
    char one_byte = 0;
    memset(_4bits, 0, sizeof(_4bits));
    unsigned int values = 0xf1f00101;
    // 11110001 11110000 00000001 00000001

    // 00010000 00010000 00001111 00011111
    for (char i = 0; i < 8; i++)
    {
        one_byte = (char)((values >> (i * 4)) & 0xf);
        strcpy(_4bits, HEX_buf[one_byte]);
        printf(_4bits);
    }
    unsigned char arras[10][2];
    printf("\r\nsizeof uint :%d", sizeof(arras));
    return 0;
}
#endif // FALSE
